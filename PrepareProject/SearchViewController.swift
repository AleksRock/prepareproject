//
//  SearchViewController.swift
//  PrepareProject
//
//  Created by Alexander Rozanov on 29.07.2020.
//  Copyright © 2020 Alexander Rozanov. All rights reserved.
//

import UIKit
import Charts

import RealmSwift

class ViewControllerTest: UIViewController,UITableViewDelegate,UITableViewDataSource,UISearchResultsUpdating {
 
    @IBOutlet weak var labelA: UILabel!
    
    @IBOutlet weak var labelB: UILabel!
  
    @IBOutlet weak var labelC: UILabel!
    
    @IBOutlet weak var labelD: UILabel!
    
    
    @IBOutlet weak var labelE: UILabel!
 
    @IBOutlet weak var labelF: UILabel!
    
    @IBOutlet weak var labelH: UILabel!
    
    @IBOutlet weak var labelI: UILabel!
    

    
    var searchController = UISearchController()
    
    var imidgePics:[String] = []
    var filterNames = [String]()
   
    @IBOutlet weak var labelLabel: UILabel!
    
    @IBOutlet weak var imageLabel: UIImageView!
    
    
    @IBOutlet weak var tableLabel: UITableView!
    
    let realm = try! Realm()
    var results: Results<NamesObject>?
    var rezultsImage: Results<ImageNamesObject>?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        searchController = ({
            
            let controller = UISearchController(searchResultsController: nil)
            controller.searchResultsUpdater = self
            controller.searchBar.sizeToFit()
            tableLabel.tableHeaderView = controller.searchBar
            
            return controller
            
        })()
        
        
        tableLabel.delegate = self
        tableLabel.dataSource = self
       
           let object = realm.objects(ImageNamesObject.self)
           for i in 0..<object.count {
                   imidgePics.append(object[i].name)
                   imidgePics[i] = object[i].name
           }
              imidgePics = imidgePics.sorted()
        
    }

    func updateSearchResults(for searchController: UISearchController) {
        let predicate = NSPredicate(format: "SELF CONTAINS[c] %@",searchController.searchBar.text!)
        filterNames = (imidgePics as NSArray).filtered(using: predicate) as! [String]
        
        tableLabel.reloadData()
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if filterNames.count > 0 {
     return   filterNames.count
        } else {
            return imidgePics.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
     
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        cell.textLabel?.text = "Example" + "\(imidgePics[indexPath.row])" //"\(arrayNest[indexPath.row])"
        
        cell.layer.borderWidth=1
        cell.layer.borderColor=UIColor.white.cgColor
        cell.backgroundColor = .systemTeal

        cell.textLabel!.textColor = .black
        
        if filterNames.count > 0 {
            
            cell.textLabel?.text = filterNames[indexPath.row]}
            else {
                cell.textLabel!.text = self.imidgePics[indexPath.row]}
            
        return cell
       
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
               
      
        let object = realm.objects(ImageNamesObject.self)
        let objectSorted = object.sorted(byKeyPath: "date")
        for i in 0..<object.count {
            if imidgePics[indexPath.row] == objectSorted[i].name {
                let realmData = objectSorted[i].image
                let dateImage = objectSorted[i].date
            let imageRealm = UIImage(data:
            realmData! as Data)
        
           imageLabel.image = imageRealm
           labelLabel.text = dateImage
                
                
                let rezultA = objectSorted[i].rezultA
                let rezultB = objectSorted[i].rezultB
                let rezultC = objectSorted[i].rezultC
                let rezultD = objectSorted[i].rezultD
                let rezultE = objectSorted[i].rezultE
                let rezultF = objectSorted[i].rezultF
                let rezultH = objectSorted[i].rezultH
                let rezultI = objectSorted[i].rezultI
                
                labelA.text = String(rezultA) + "%"
                labelB.text = String(rezultB) + "%"
                labelC.text = String(rezultC) + "%"
                labelD.text = String(rezultD) + "%"
                labelE.text = String(rezultE) + "%"
                labelF.text = String(rezultF) + "%"
                labelH.text = String(rezultH) + "%"
                labelI.text = String(rezultI) + "%"
                
            }
  }
}
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
          if editingStyle == .delete {
              // Delete the row from the data source
             
      
            
              //это структура, характерная для Results
            rezultsImage = realm.objects(ImageNamesObject.self)
            let rezultsImageSorted = rezultsImage?.sorted(byKeyPath: "date")
              
                         try! self.realm.write {
                          self.realm.delete((rezultsImageSorted?[indexPath.row])!)
                            }
     
            results = realm.objects(NamesObject.self)
            let resultsSorted = results?.sorted(byKeyPath: "date")
                       try! self.realm.write {
                        self.realm.delete((resultsSorted?[indexPath.row])!)
                          }
            
                  imidgePics.remove(at: indexPath.row)
              

              
              tableView.deleteRows(at: [indexPath], with: .fade)
          } else if editingStyle == .insert {
              // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
              
              tableView.reloadData()
          }
      }
    
}
