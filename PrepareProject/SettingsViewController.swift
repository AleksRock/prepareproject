//
//  SettingsViewController.swift
//  PrepareProject
//
//  Created by Alexander Rozanov on 28.07.2020.
//  Copyright © 2020 Alexander Rozanov. All rights reserved.
//

import UIKit

class SettingsViewController: UIViewController {

    override func viewDidLoad() {
            super.viewDidLoad()

            // Do any additional setup after loading the view.
            
            
            UIApplication.openPhoneSettings { isSuccess in
                 if isSuccess == false {
                      //Display error
                 }
            }
        }
        

        /*
        // MARK: - Navigation

        // In a storyboard-based application, you will often want to do a little preparation before navigation
        override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
            // Get the new view controller using segue.destination.
            // Pass the selected object to the new view controller.
        }
        */

    }
    extension UIApplication {


        static func openAppSettings(completion: @escaping (_ isSuccess: Bool) -> ()) {
            guard let url = URL(string: UIApplication.openSettingsURLString) else {
                completion(false)
                return
            }



            let app = UIApplication.shared



            app.open(url) { isSuccess in
                completion(isSuccess)
            }
        }



        static func openPhoneSettings(completion: @escaping (_ isSuccess: Bool) -> ()) {
            guard let url = URL(string: "App-Prefs:root=General") else {
                completion(false)
                return
            }



            let app = UIApplication.shared



            app.open(url) { isSuccess in
                completion(isSuccess)
            }
        }



    }
